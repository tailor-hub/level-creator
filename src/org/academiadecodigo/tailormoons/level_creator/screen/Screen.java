package org.academiadecodigo.tailormoons.level_creator.screen;

import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;
import org.academiadecodigo.tailormoons.level_creator.handler.mouse.MouseListener;
import org.academiadecodigo.tailormoons.level_creator.screen.level.Level;
import org.academiadecodigo.tailormoons.level_creator.screen.toolbar.ToolBar;

public class Screen {

    public static final int ICON_SIZE = 20;

    public Canvas canvas;
    private Level level;
    private ToolBar toolBar;
    private MouseListener mouseListener;

    private final int levelWidth = 1280;
    private final int levelHeight = 720;
    private final int paddingWidth = 10;
    private final int toolbarWidth = 250;

    private boolean button1Pressed;


    public Screen() {
        canvas = Canvas.getInstance();
        canvas.setTitle("Screen Creator");
        canvas.setIcon("assets/appIcon.png");
        canvas.getFrame().setSize(levelWidth + paddingWidth + toolbarWidth, levelHeight);
    }


    public void init() {
        level = new Level();

        toolBar = new ToolBar();
        toolBar.init();

        mouseListener = new MouseListener(this);
    }


    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == 1) {

            return;
        }

        if (mouseEvent.getButton() == 3) {

            return;
        }
    }


    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == 1) {
            button1Pressed = !button1Pressed;
            if (button1Pressed) {
                if (mouseEvent.getX() > levelWidth) {
                    toolbarClick((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    return;
                }
                levelClick((int) mouseEvent.getX(), (int) mouseEvent.getY());
            }
            return;
        }

        if (mouseEvent.getButton() == 3) {

            return;
        }
    }


    public void mouseDragged(MouseEvent mouseEvent) {

    }


    private void toolbarClick(int x, int y) {
        for (GameObject icon : toolBar.getIcons()) {
            if (    x >= icon.getPosition().getX() &&
                    x <= icon.getPosition().getX() + ICON_SIZE &&
                    y >= icon.getPosition().getY() &&
                    y <= icon.getPosition().getY() + ICON_SIZE)
            {
                System.out.println(icon.getClass().getName());

            }
        }
    }


    private void levelClick(int x, int y) {

    }

}
