package org.academiadecodigo.tailormoons.level_creator.screen.toolbar;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;
import org.academiadecodigo.tailormoons.level_creator.gameObject.enemy.EnemyEnum;
import org.academiadecodigo.tailormoons.level_creator.gameObject.item.ItemEnum;
import org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous.MiscellaneousEnum;
import org.academiadecodigo.tailormoons.level_creator.gameObject.structure.StructureEnum;

import java.util.LinkedList;
import java.util.List;

public class ToolBar {

    private final Rectangle padding;
    private final Rectangle toolBar;

    private final List<GameObject> icons = new LinkedList<>();


    public ToolBar() {
        padding = new Rectangle(1280, 0, 10, 720);
        padding.setColor(Color.LIGHT_GRAY);
        padding.fill();

        toolBar = new Rectangle(1280 + padding.getWidth(), 0, 250, 720);
        toolBar.draw();
    }


    public void init() {
        initLabels();
        initIcons();
    }


    private void initLabels() {
        Text structuresLabel = new Text(0, 20, "Structures");
        structuresLabel.translate(toolBar.getX() + (int) ((toolBar.getWidth() - structuresLabel.getWidth()) / 2), 0);

        Text enemiesLabel = new Text(0, 200, "Enemies");
        enemiesLabel.translate(toolBar.getX() + (int) ((toolBar.getWidth() - enemiesLabel.getWidth()) / 2), 0);

        Text itemsLabel = new Text(0, 350, "Items");
        itemsLabel.translate(toolBar.getX() + (int) ((toolBar.getWidth() - itemsLabel.getWidth()) / 2), 0);

        Text miscellaneousLabel = new Text(0, 500, "Miscellaneous");
        miscellaneousLabel.translate(toolBar.getX() + (int) ((toolBar.getWidth() - miscellaneousLabel.getWidth()) / 2), 0);

        structuresLabel.draw();
        enemiesLabel.draw();
        itemsLabel.draw();
        miscellaneousLabel.draw();
    }


    private void initIcons() {

        int maxCol = 3;
        int paddingIcon = 5;
        int iconSize = 20;

        int posX;
        int posY;
        int counter;

        //PLACE STRUCTURE ICONS
        //Calc to center icons on toolBar
        posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
        posY = 50;
        counter = 0;

        for (StructureEnum structure : StructureEnum.values()) {

            icons.add(structure.getGameObjectFactory().create(posX, posY));

            posX += paddingIcon + iconSize;
            counter++;
            if (counter >= maxCol) {
                posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
                posY += paddingIcon + iconSize;
                counter = 0;
            }

        }

        //PLACE ENEMY ICONS
        posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
        posY = 230;
        counter = 0;

        for (EnemyEnum enemyEnum : EnemyEnum.values()) {

            icons.add(enemyEnum.getGameObjectFactory().create(posX, posY));

            posX += paddingIcon + iconSize;
            counter++;
            if (counter >= maxCol) {
                posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
                posY += paddingIcon + iconSize;
                counter = 0;
            }

        }

        //PLACE ITEM ICONS
        posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
        posY = 380;
        counter = 0;

        for (ItemEnum itemEnum : ItemEnum.values()) {

            icons.add(itemEnum.getGameObjectFactory().create(posX, posY));

            posX += paddingIcon + iconSize;
            counter++;
            if (counter >= maxCol) {
                posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
                posY += paddingIcon + iconSize;
                counter = 0;
            }
        }

        //PLACE MISCELLANEOUS ICONS
        posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
        posY = 530;
        counter = 0;

        for (MiscellaneousEnum miscellaneousEnum : MiscellaneousEnum.values()) {

            icons.add(miscellaneousEnum.getGameObjectFactory().create(posX, posY));

            posX += paddingIcon + iconSize;
            counter++;
            if (counter >= maxCol) {
                posX = toolBar.getX() + (toolBar.getWidth() - (maxCol * iconSize) - ((maxCol - 1) * paddingIcon)) / 2;
                posY += paddingIcon + iconSize;
                counter = 0;
            }
        }


        for (GameObject icon : icons) {
            icon.draw();
        }

    }


    public List<GameObject> getIcons() {
        return icons;
    }

}