package org.academiadecodigo.tailormoons.level_creator.screen.level;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Level {

    private Picture picture;

    public Level() {
        Rectangle rectangle = new Rectangle(0, 0, 1280, 720);
        picture = new Picture(0, 0, "assets/backgroundLevel1.jpg");
        picture.draw();
    }

}
