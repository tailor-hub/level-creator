package org.academiadecodigo.tailormoons.level_creator;

import org.academiadecodigo.tailormoons.level_creator.screen.Screen;


public class Main {

    public static void main(String[] args) {

        Screen screen = new Screen();
        screen.init();

    }

}
