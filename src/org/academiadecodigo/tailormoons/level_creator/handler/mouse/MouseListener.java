package org.academiadecodigo.tailormoons.level_creator.handler.mouse;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;
import org.academiadecodigo.tailormoons.level_creator.screen.Screen;

public class MouseListener implements MouseHandler {

    private final Screen screen;


    public MouseListener(Screen screen) {
        this.screen = screen;
        init();
    }


    private void init() {
        Mouse mouse = new Mouse(this);
        mouse.addEventListener(MouseEventType.MOUSE_CLICKED);
        mouse.addEventListener(MouseEventType.MOUSE_PRESSED);
        mouse.addEventListener(MouseEventType.MOUSE_RELEASED);
        mouse.addEventListener(MouseEventType.MOUSE_MOVED);
        mouse.addEventListener(MouseEventType.MOUSE_DRAGGED);
    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        screen.mouseClicked(mouseEvent);
    }


    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        screen.mousePressed(mouseEvent);
    }


    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        System.out.println(mouseEvent);
    }


    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }


    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        System.out.println(mouseEvent);
    }

}
