package org.academiadecodigo.tailormoons.level_creator.gameObject.item;

import org.academiadecodigo.tailormoons.level_creator.gameObject.Factory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObjectFactory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.Placeable;

import java.io.File;

public enum ItemEnum implements Placeable {
    KEY(Factory::createKey);

    private final GameObjectFactory gameObjectFactory;


    ItemEnum(GameObjectFactory gameObjectFactory) {
        this.gameObjectFactory = gameObjectFactory;
    }


    @Override
    public GameObjectFactory getGameObjectFactory() {
        return gameObjectFactory;
    }

}
