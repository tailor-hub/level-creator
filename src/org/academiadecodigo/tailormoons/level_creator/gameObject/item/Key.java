package org.academiadecodigo.tailormoons.level_creator.gameObject.item;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Key extends GameObject {

    public static final String ICON_PATH = "key.png";

    public Key(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
