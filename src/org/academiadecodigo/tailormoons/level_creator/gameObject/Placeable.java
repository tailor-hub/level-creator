package org.academiadecodigo.tailormoons.level_creator.gameObject;

public interface Placeable {

    GameObjectFactory getGameObjectFactory();

}
