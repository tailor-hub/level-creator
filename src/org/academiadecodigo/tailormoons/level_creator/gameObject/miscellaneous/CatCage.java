package org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class CatCage extends GameObject {

    public static final String ICON_PATH = "catCage.png";


    public CatCage(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
