package org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Player extends GameObject {

    public static final String ICON_PATH = "player.png";


    public Player(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
