package org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous;

import org.academiadecodigo.tailormoons.level_creator.gameObject.Factory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObjectFactory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.Placeable;

import java.io.File;

public enum MiscellaneousEnum implements Placeable {
    PLAYER(Factory::createPlayer),
    CAT_CAGE(Factory::createCatCage);

    private final GameObjectFactory gameObjectFactory;


    MiscellaneousEnum(GameObjectFactory gameObjectFactory) {
        this.gameObjectFactory = gameObjectFactory;
    }


    @Override
    public GameObjectFactory getGameObjectFactory() {
        return gameObjectFactory;
    }

}
