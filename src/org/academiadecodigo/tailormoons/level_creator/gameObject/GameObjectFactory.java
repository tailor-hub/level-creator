package org.academiadecodigo.tailormoons.level_creator.gameObject;

public interface GameObjectFactory {

    GameObject create(int x, int y);

}
