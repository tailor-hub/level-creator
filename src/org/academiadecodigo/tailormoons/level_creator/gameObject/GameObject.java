package org.academiadecodigo.tailormoons.level_creator.gameObject;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.level_creator.screen.Position;

import java.io.File;

public abstract class GameObject {

    private Picture picture;
    private Position position;


    public GameObject(int x, int y, String imagePath) {
        String path = "assets/icons/" + imagePath;
        File image = new File(path);
        if (!image.exists()) {
            path = "assets/icons/404.png";
        }
        picture = new Picture(x, y, path);
        position = new Position(x, y);
    }


    public void draw() {
        picture.draw();
    }


    public Position getPosition() {
        return position;
    }

}
