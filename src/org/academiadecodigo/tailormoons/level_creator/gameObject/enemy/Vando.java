package org.academiadecodigo.tailormoons.level_creator.gameObject.enemy;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Vando extends GameObject {

    public static final String ICON_PATH = "vando.png";


    public Vando(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
