package org.academiadecodigo.tailormoons.level_creator.gameObject.enemy;

import org.academiadecodigo.tailormoons.level_creator.gameObject.Factory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObjectFactory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.Placeable;

public enum EnemyEnum implements Placeable {
    SID(Factory::createSid),
    VANDO(Factory::createVando),
    FRANCISCO(Factory::createFrancisco);

    private final GameObjectFactory gameObjectFactory;


    EnemyEnum(GameObjectFactory gameObjectFactory) {
        this.gameObjectFactory = gameObjectFactory;
    }


    @Override
    public GameObjectFactory getGameObjectFactory() {
        return gameObjectFactory;
    }

}
