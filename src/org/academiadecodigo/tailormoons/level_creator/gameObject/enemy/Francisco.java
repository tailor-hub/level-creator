package org.academiadecodigo.tailormoons.level_creator.gameObject.enemy;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Francisco extends GameObject {

    public static final String ICON_PATH = "francisco.png";


    public Francisco(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
