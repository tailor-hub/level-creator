package org.academiadecodigo.tailormoons.level_creator.gameObject.enemy;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Sid extends GameObject {

    public static final String ICON_PATH = "sid.png";


    public Sid(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
