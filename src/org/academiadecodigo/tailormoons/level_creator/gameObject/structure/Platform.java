package org.academiadecodigo.tailormoons.level_creator.gameObject.structure;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Platform extends GameObject {

    public static final String ICON_PATH = "platform.jpg";


    public Platform(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
