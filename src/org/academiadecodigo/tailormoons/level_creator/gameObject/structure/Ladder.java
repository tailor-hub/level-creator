package org.academiadecodigo.tailormoons.level_creator.gameObject.structure;

import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;

public class Ladder extends GameObject {

    public static final String ICON_PATH = "ladder.png";


    public Ladder(int x, int y) {
        super(x, y, ICON_PATH);
    }

}
