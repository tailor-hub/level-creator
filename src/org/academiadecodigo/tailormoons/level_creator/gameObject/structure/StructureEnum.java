package org.academiadecodigo.tailormoons.level_creator.gameObject.structure;

import org.academiadecodigo.tailormoons.level_creator.gameObject.Factory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObject;
import org.academiadecodigo.tailormoons.level_creator.gameObject.GameObjectFactory;
import org.academiadecodigo.tailormoons.level_creator.gameObject.Placeable;

import java.io.File;

public enum StructureEnum implements Placeable {
    PLATFORM(Factory::createPlatform),
    LADDER(Factory::createLadder);

    private final GameObjectFactory gameObjectFactory;


    StructureEnum(GameObjectFactory gameObjectFactory) {
        this.gameObjectFactory = gameObjectFactory;
    }


    @Override
    public GameObjectFactory getGameObjectFactory() {
        return gameObjectFactory;
    }

}
