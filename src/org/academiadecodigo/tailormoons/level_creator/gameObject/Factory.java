package org.academiadecodigo.tailormoons.level_creator.gameObject;

import org.academiadecodigo.tailormoons.level_creator.gameObject.enemy.Francisco;
import org.academiadecodigo.tailormoons.level_creator.gameObject.enemy.Sid;
import org.academiadecodigo.tailormoons.level_creator.gameObject.enemy.Vando;
import org.academiadecodigo.tailormoons.level_creator.gameObject.item.Key;
import org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous.CatCage;
import org.academiadecodigo.tailormoons.level_creator.gameObject.miscellaneous.Player;
import org.academiadecodigo.tailormoons.level_creator.gameObject.structure.Ladder;
import org.academiadecodigo.tailormoons.level_creator.gameObject.structure.Platform;

public class Factory {

    public static GameObject createPlayer(int x, int y) {
        return new Player(x, y);
    }


    public static GameObject createCatCage(int x, int y) {
        return new CatCage(x, y);
    }


    public static GameObject createKey(int x, int y) {
        return new Key(x, y);
    }


    public static GameObject createLadder(int x, int y) {
        return new Ladder(x, y);
    }


    public static GameObject createPlatform(int x, int y) {
        return new Platform(x, y);
    }


    public static GameObject createVando(int x, int y) {
        return new Vando(x, y);
    }


    public static GameObject createSid(int x, int y) {
        return new Sid(x, y);
    }


    public static GameObject createFrancisco(int x, int y) {
        return new Francisco(x, y);
    }

}
